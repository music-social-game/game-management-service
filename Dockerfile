FROM python:latest

ENV PYTHONUNBUFFERED 1
RUN apt update && apt install git libxml2 libxml2-dev libxslt-dev gcc python3-dev musl-dev -y

RUN mkdir -p /code

WORKDIR /code

COPY Pipfile Pipfile.lock /code/

RUN pip install pipenv
RUN pipenv install --system

COPY ./app /code/app/

WORKDIR /code/
