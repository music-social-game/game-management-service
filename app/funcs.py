import aio_pika
import motor.motor_asyncio
import json
from config import MONGO_URL
from schema import NewGame

mongo_client = motor.motor_asyncio.AsyncIOMotorClient(MONGO_URL)
db = mongo_client.Games


async def send_result(result, message: aio_pika.IncomingMessage, exchange: aio_pika.Exchange):
    if result:
        data = {"result": "OK"}
    else:
        data = {"result": "NOT OK"}
    await exchange.publish(
        aio_pika.Message(
            body=json.dumps(data).encode(),
            correlation_id=message.correlation_id
        ),
        routing_key=message.reply_to,
    )


async def create_game_room(game: NewGame, message: aio_pika.IncomingMessage, exchange: aio_pika.Exchange):
    data = game.dict()
    data['players'] = [game.user.dict()]
    data['current_round'] = 0
    db_game = await db.games.find_one({"game_key": game.game_key})
    if db_game:
        await send_result(False, message, exchange)
        return
    result = await db.games.insert_one(data)
    await send_result(result, message, exchange)
