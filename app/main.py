import asyncio
import aio_pika
import json
from config import MQ_URL, settings, MONGO_URL
from schema import NewGame
from funcs import create_game_room
from functools import partial

func_mapping = {
    "create_game_room": {'func': create_game_room, 'args': [NewGame]},
}


async def on_message(exchange: aio_pika.Exchange, message: aio_pika.IncomingMessage):
    async with message.process():
        body = json.loads(message.body)
        action = body.pop('action', None)
        if action:
            func_dict = func_mapping[action]
            func = func_dict['func']
            args = [schema(**body[schema.__name__]) for schema in func_dict['args']]
            await func(*args, message=message, exchange=exchange)


async def main():
    attempt = 10
    while True:
        try:
            attempt -= 1
            mq_connection = await aio_pika.connect_robust(MQ_URL)
            break
        except ConnectionError:
            if attempt > 0:
                print("Reestablish connection to MQ")
                await asyncio.sleep(10)
            else:
                raise
    channel = await mq_connection.channel()
    music_exchange = await channel.get_exchange("music")
    queue = await channel.declare_queue(durable=True)
    await queue.bind(music_exchange, routing_key=settings.music_queue_key)
    await queue.consume(partial(on_message, music_exchange))



if __name__ == "__main__":
    print("WAITING FOR MESSAGES")
    loop = asyncio.get_event_loop()
    loop.create_task(main())
    loop.run_forever()


