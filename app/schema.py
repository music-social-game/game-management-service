from pydantic import BaseModel, EmailStr, UUID4
from typing import Optional


class UserShort(BaseModel):
    id: str
    username: str
    email: Optional[EmailStr]


class NewGame(BaseModel):
    game_key: str
    user: UserShort
