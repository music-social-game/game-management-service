from pydantic import BaseSettings
from pathlib import Path

ENV_PATH = Path.cwd() / '.env'


class Settings(BaseSettings):
    host: str
    mq_host:  str
    mq_port:  int
    mq_user:  str
    mq_pass:  str
    spotify_client_id:  str
    spotify_secret:  str
    session_middleware_key:  str
    music_queue_key:  str
    mongo_host:  str
    mongo_port:  int
    mongo_initdb_root_username:  str
    mongo_initdb_root_password:  str

    class Config:
        env_file = ENV_PATH


settings = Settings()

MQ_URL = f"amqp://{settings.mq_user}:{settings.mq_pass}@{settings.mq_host}:{settings.mq_port}/"
MONGO_URL = f"mongodb://{settings.mongo_initdb_root_username}:{settings.mongo_initdb_root_password}@{settings.mongo_host}:{settings.mongo_port}/"